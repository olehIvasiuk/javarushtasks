package ua.javarush.task.task24.task2408;

import java.util.Date;

public class Dog implements Pet {
    private String name;

    public Dog(String name) {
        this.name = name;
    }

    /**
     * Якщо так вийшло, що є готовий клас А (тут SuperDog) з логікою, яку ви хочете використати.
     * То можливі 3 способи:
     * 1) з класу А скопіювати логіку собі (це погано, тому що підтримувати кілька копій одного й того ж коду проблематично)
     * 2) створити екземпляр класу А всередині нашого класу і використовувати його, зв'язок has-a (не завжди підходить, тому що клас А сам по собі)
     * 3) створити внутрішній клас, який успадковується від А, використовувати його методи разом з
     * методами та полями нашого класу, т.к. внутрішній клас має доступ до свого батька як спадкоємець,
     * а також до всіх полів і методів, включаючи private того класу, в який він вкладений.
     * <p/>
     * Отже, розуміємося з п.3:
     * Усередині методу toSayable створіть class DogPet, який успадковується від SuperDog та реалізує інтерфейс Sayable
     * створіть метод private String getName(), який використовуватиме логіку двох класів - Dog і SuperDog.
     * Нехай цей метод повертає ім'я собаки (клас Dog), яке по обидва боки виділено getSuperQuotes (клас SuperDog)
     * Приклад, "*** Барбос ***"
     * Логіка методу say:
     * Якщо i < 1, то використовуючи метод getName вивести на екран, що собака спить. Наприклад, " *** Шарик *** спить."'
     * Інакше вивести фразу: "ім'я_собаки гавкає гааав! сьогоднішня_дата". Приклад для i = 3, "*** Тузик *** гавкає гааав! 13-лис-2013 Ср"
     * Для форматування дати використовуйте formatter із класу SuperDog.
     * <p/>
     * <b>Приклад виведення:</b>
     * *** Барбос *** гавкає гааааав! 13-лис.-2013 порівн.
     * *** Тузик *** гавкає гаав! 13-лис.-2013 порівн.
     * *** Бобик *** гавкає гааав! 13-лис.-2013 порівн.
     * Миша пищить.
     * *** Шарик *** спить.
     *
     * @param i кількість букв 'а' у слові гав
     * @return екземпляр класу DogPet
     */
    public Sayable toSayable(final int i) {

        class DogPet extends SuperDog implements Sayable {

            private String getName() {
                return this.getSuperQuotes() + Dog.this.name + this.getSuperQuotes();
            }

            @Override
            public String say() {
                StringBuilder sb = new StringBuilder(getName()).append(" гавкає г");
                if (i < 1) {
                    return getName() + " спить.";
                }
                for (int j = 0; j < i; j++) {
                    sb.append("а");
                }
                sb.append("в!");
                sb.append(" ");
                sb.append(formatter.format(new Date()));
                return sb.toString();
            }
        }

        return new DogPet();
    }
}