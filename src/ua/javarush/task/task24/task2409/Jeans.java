package ua.javarush.task.task24.task2409;

public interface Jeans extends Item{
    int getSize();
    int getLength();
    String getTM();
}
