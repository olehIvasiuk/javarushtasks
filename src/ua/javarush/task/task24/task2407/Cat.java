package ua.javarush.task.task24.task2407;

import java.util.stream.Stream;

public class Cat implements Pet {
    private String name;

    public Cat(String name) {
        this.name = name;
    }
    /**
     * Це - механізм адаптування до іншого інтерфейсу - Sayable
     * Усередині методу toSayable створіть class CatPet, що реалізує інтерфейс Sayable
     * Логіка методу say:
     * Якщо i < 1, то вивести на екран, що кіт спить. Наприклад, "Васька спить."
     * Інакше вивести фразу: "ім'я_кота каже мяу!". Приклад для i=3, "Васька каже мяяяу!"
     * <p/>
     * <b>Приклад виведення:</b>
     * Мурзік спить.
     * Васька каже мяяу!
     * Кішка каже мяяяяяу!
     * Миша пищить.
     * Томас каже мяу!
     * <p/>
     *
     * @param i кількість літер 'я' у слові мяу
     * @return екземпляр класу CatPet
     */
    public Sayable toSayable(final int i) {
        class CatPet implements Sayable {
//           static String duplicate = "я";

            @Override
            public String say() {
                if (i < 1){
                    return String.format("%s спить.",name);
                }else {
                    StringBuilder sb = new StringBuilder(Cat.this.name).append(" каже м");
                    for (int j = 0; j < i; j++)
                        sb.append("я");
                    sb.append("у!");
                    return sb.toString();
                }
            }
        }
        return new CatPet();
    }
}