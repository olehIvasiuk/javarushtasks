package ua.javarush.task.task28.task2810;

import ua.javarush.task.task28.task2810.model.*;

import ua.javarush.task.task28.task2810.model.LinkedinStrategy;
import ua.javarush.task.task28.task2810.model.Provider;
import ua.javarush.task.task28.task2810.view.HtmlView;

public class Aggregator {

    public static void main(String[] args) {
        HtmlView view = new HtmlView();
        Model model = new Model(view, new Provider(new LinkedinStrategy()), new Provider(new IndeedStrategy()));
        Controller controller = new Controller(model);

        view.setController(controller);

        view.emulateCitySelection();
    }
}
