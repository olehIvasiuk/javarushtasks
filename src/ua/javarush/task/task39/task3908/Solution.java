package ua.javarush.task.task39.task3908;

/* 
Чи можливий паліндром?
*/

import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        isPalindromePermutation(scanner.nextLine());
    }

    public static boolean isPalindromePermutation(String s) {
        char[] chars = s.toLowerCase().toCharArray();
        Set<Character> map = new HashSet<>();
        for (Character c : chars) {
            map.add(c);
        }
        for (Character character : map) {
            int count = 0;
            for (int j = 0; j < chars.length; j++) {
                if (count > 1) {
                    return false;
                }
                if (character == chars[j]) {
                    count++;
                }
            }
        }
        return true;
    }
}

