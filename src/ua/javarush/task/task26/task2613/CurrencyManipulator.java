package ua.javarush.task.task26.task2613;

import ua.javarush.task.task26.task2613.exception.NotEnoughMoneyException;

import java.util.*;

public class CurrencyManipulator {
    private String currencyCode;
    private Map<Integer, Integer> denominations = new TreeMap<>(Comparator.reverseOrder());

    public CurrencyManipulator(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void addAmount(int denomination, int count) {
        if (denominations.containsKey(denomination)) {
            denominations.put(denomination, denominations.get(denomination) + count);
        } else {
            denominations.put(denomination, count);
        }
    }

    public int getTotalAmount() {
        return denominations.entrySet().stream()
                .mapToInt(entry -> (entry.getKey() * entry.getValue()))
                .sum();
    }

    public boolean hasMoney() {
        return !denominations.isEmpty();
    }

    public boolean isAmountAvailable(int expectedAmount) {
        return getTotalAmount() >= expectedAmount;
    }

    public Map<Integer, Integer> withdrawAmount(int expectedAmount) throws NotEnoughMoneyException {
        Map<Integer, Integer> copyOfDen = new TreeMap<>(denominations);
        List<Integer> denomination = new ArrayList<>(copyOfDen.keySet());
        denomination.sort(Collections.reverseOrder());

        Map<Integer, Integer> result = new TreeMap<>();

        for (int i = 0; i < denomination.size(); ) {
            int nom = denomination.get(i);

            if (expectedAmount >= nom) {
                if (copyOfDen.get(nom) > 0) {
                    expectedAmount -= nom;
                    result.put(nom, result.get(nom) == null ? 1 : result.get(nom) + 1);
                } else {
                    i++;
                    continue;
                }
            }

            if (expectedAmount == 0) {
                break;
            }
            if (expectedAmount < nom) {
                i++;
            }
        }
        if (expectedAmount != 0) {
            throw new NotEnoughMoneyException();
        }
        for (Map.Entry<Integer, Integer> entry : result.entrySet()) {
            int key = entry.getKey();
            int value = entry.getValue();
            denominations.put(key, denominations.get(key) - value);
        }
        return result;
    }
}
