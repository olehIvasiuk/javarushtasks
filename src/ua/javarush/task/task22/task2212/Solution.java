package ua.javarush.task.task22.task2212;

/* 
Перевірка номера телефону
*/

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {
    static String s1 = "+380501234567";
    static String s2 = "+38(050)1234567";
    static String s3 = "(050)1234567";
    static String s4 = "0(501)234567";
    static String s5 = "+38)050(1234567";
    static String s6 = "+38(050)123-45-67";
    static String s7 = "050ххх4567";
    static String s8 = "050123456";
    static String s9 = "(0)501234567";


    public static boolean checkTelNumber(String telNumber) {
        if (telNumber == null) {
            return false;
        } else {
            return (telNumber.matches("^\\+(\\d[()]?){12}$") || telNumber.matches("^([()]?\\d){10}$"))
                    && telNumber.matches("^(\\+)?(\\d+)?(\\(\\d{3}\\))?\\d+$");
        }

    }

    public static void main(String[] args) {
        checkTelNumber(s1);
        checkTelNumber(s2);
        checkTelNumber(s3);
        checkTelNumber(s4);
        checkTelNumber(s5);
        checkTelNumber(s6);
        checkTelNumber(s7);
        checkTelNumber(s8);
        checkTelNumber(s9);

    }
}
