package ua.javarush.task.task38.task3812;

/* 
Обробка анотацій
*/

public class Solution {
    public static void main(String[] args) {
        printFullyQualifiedNames(Solution.class);
        printFullyQualifiedNames(SomeTest.class);

        printValues(Solution.class);
        printValues(SomeTest.class);
    }

    public static boolean printFullyQualifiedNames(Class c) {
        try {
            PrepareMyTest prepareMyTest = (PrepareMyTest) c.getAnnotation(PrepareMyTest.class);
            for (String fullyQualifiedName : prepareMyTest.fullyQualifiedNames()) {
                System.out.println(fullyQualifiedName);
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static boolean printValues(Class c) {
        if (!c.isAnnotationPresent(PrepareMyTest.class)) {
            return false;
        }
        PrepareMyTest test = (PrepareMyTest) c.getAnnotation(PrepareMyTest.class);
        for (Class clazz : test.value()) {
            System.out.println(clazz);
        }
        System.out.println("values");
        return true;
    }
}
