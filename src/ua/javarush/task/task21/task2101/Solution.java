package ua.javarush.task.task21.task2101;

/* 
Визначаємо адресу мережі
*/

import java.util.Arrays;
import java.util.Objects;

public class Solution {
    public static void main(String[] args) {
        byte[] ip = new byte[]{(byte) 192, (byte) 168, 1, 2};
        byte[] mask = new byte[]{(byte) 255, (byte) 255, (byte) 254, 0};
        byte[] netAddress = getNetAddress(ip, mask);

        System.out.println(Arrays.toString(ip));
        System.out.println(Arrays.toString(mask));
        System.out.println(Arrays.toString(netAddress));


        print(ip);          //11000000 10101000 00000001 00000010
        print(mask);        //11111111 11111111 11111110 00000000
        print(netAddress);  //11000000 10101000 00000000 00000000
    }

    public static byte[] getNetAddress(byte[] ip, byte[] mask) {
        return new byte[4];
    }

    public static void print(byte[] bytes) {
        for (byte b : bytes){
            System.out.println(String.format("%8s", Integer.toBinaryString(b & 0xFF)).replace(" ", "0"));
        }
    }
}
