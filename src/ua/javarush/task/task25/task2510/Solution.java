package ua.javarush.task.task25.task2510;

/* 
Поживемо — побачимо
*/

public class Solution extends Thread {

    public Solution() {
        this.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable throwable) {
                String message = null;
                if (throwable instanceof Error) {
                    message = "Не можна працювати далі";
                }else if (throwable instanceof Exception){
                    message = "Треба обробити";
                }else if (throwable instanceof Throwable){
                    message = "Поживемо - побачимо";
                }
                System.out.println(message);
            }
        });
    }

    public static void main(String[] args) {

    }
}
