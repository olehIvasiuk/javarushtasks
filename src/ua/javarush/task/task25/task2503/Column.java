package ua.javarush.task.task25.task2503;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public enum Column implements Columnable{
    Customer("Customer"),
    BankName("Bank Name"),
    AccountNumber("Account Number"),
    Amount("Available Amount");

    private String columnName;

    private static int[] realOrder;

    private Column(String columnName) {
        this.columnName = columnName;
    }

    /**
     * Задає новий порядок відображення колонок, що зберігається в масиві реальноїOrder.
     * realOrder[індекс в энуме] = порядок відображення; -1, якщо колонка не відображається.
     *
     * @param newOrder нова послідовність колонок, в якій вони відображатимуться в таблиці
     * @throws IllegalArgumentException при дублікаті колонки
     */
    public static void configureColumns(Column... newOrder) {
        realOrder = new int[values().length];
        for (Column column : values()) {
            realOrder[column.ordinal()] = -1;
            boolean isFound = false;

            for (int i = 0; i < newOrder.length; i++) {
                if (column == newOrder[i]) {
                    if (isFound) {
                        throw new IllegalArgumentException("Column '" + column.columnName + "' is already configured.");
                    }
                    realOrder[column.ordinal()] = i;
                    isFound = true;
                }
            }
        }
    }

    /**
     * Обчислює та повертає список відображених колонок у налаштованому порядку (див. метод configureColumns)
     * Використовується поле realOrder.
     *
     * @return список колонок
     */
    public static List<Column> getVisibleColumns() {
        List<Column> result = new LinkedList<>();
        int nextIndex = 0;
        boolean hasNextElement = true;
        while (hasNextElement) {
            hasNextElement = false;
            for (int i = 0; i < realOrder.length; i++) {
                if (realOrder[i] == nextIndex) {
                    result.add(values()[i]);
                    break;
                }
            }
            for (int i = 0; i < realOrder.length; i++) {
                if (realOrder[i] == nextIndex + 1) {
                    hasNextElement = true;
                    nextIndex++;
                    break;
                }
            }
        }
        return result;
    }

    @Override
    public String getColumnName() {
        return columnName;
    }

    @Override
    public boolean isShown() {
        return realOrder[this.ordinal()] != -1;
    }

    @Override
    public void hide() {
        int oldOrder = realOrder[ordinal()];
        if (oldOrder == -1) return;
        realOrder[ordinal()] = -1;
        for (int i = 0; i < realOrder.length; i++) {
            int currentIndex = realOrder[i];
            if (currentIndex != -1 && currentIndex > oldOrder) {
                realOrder[i] -= 1;
            }
        }
    }
}
