package ua.javarush.task.task36.task3608;

import ua.javarush.task.task36.task3608.controller.Controller;
import ua.javarush.task.task36.task3608.model.FakeModel;
import ua.javarush.task.task36.task3608.model.MainModel;
import ua.javarush.task.task36.task3608.model.Model;
import ua.javarush.task.task36.task3608.view.EditUserView;
import ua.javarush.task.task36.task3608.view.UsersView;

public class Solution {
    public static void main(String[] args) {
        Model model = new MainModel();
        UsersView usersView = new UsersView();
        EditUserView editUserView = new EditUserView();
        Controller controller = new Controller();

        usersView.setController(controller);
        editUserView.setController(controller);
        controller.setModel(model);
        controller.setUsersView(usersView);
        controller.setEditUserView(editUserView);

        usersView.fireEventShowAllUsers();
        usersView.fireEventOpenUserEditForm(126l);
        editUserView.fireEventUserDeleted(124l);
        editUserView.fireEventUserChanged("NewName", 126L, 73);
        usersView.fireEventShowDeletedUsers();
    }
}