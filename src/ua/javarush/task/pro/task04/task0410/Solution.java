package ua.javarush.task.pro.task04.task0410;

import java.util.Scanner;

/* 
Друге мінімальне число серед уведених
*/

public class Solution {
    public static void main(String[] args) {
        //напишіть тут ваш код
        Scanner input = new Scanner(System.in);
        int min = Integer.MAX_VALUE;
        int preMin = 0;
        boolean isFirst = true;
        while(input.hasNextInt()){
            int x = input.nextInt();
            if(isFirst){
                min = x;
                isFirst = false;
                continue;
            }
            if(min == x){
                continue;
            }
            if(x < min ){
                preMin = min;
                min = x;
            }else if(x < preMin){
                preMin = x;
            }
            
     
        }
        System.out.println(preMin);
        
        
        
    }
}