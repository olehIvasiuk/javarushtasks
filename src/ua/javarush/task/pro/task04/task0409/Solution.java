package ua.javarush.task.pro.task04.task0409;

import java.util.Scanner;

/* 
Мінімум серед уведених чисел
*/

public class Solution {
    public static void main(String[] args) {
        //напишіть тут ваш код
        Scanner input = new Scanner(System.in);
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        
        while (input.hasNextInt()){
            
            int x = input.nextInt();
        
            if(x < min){
            min = x;
            }else if(x > max){
                max = x;
            }
        }
            System.out.println(min);

    }
}