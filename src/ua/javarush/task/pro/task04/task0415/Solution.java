package ua.javarush.task.pro.task04.task0415;

import java.util.Scanner;

/* 
Площа круга
*/

public class Solution {
    public static void main(String[] args) {
        //напишіть тут ваш код
        Scanner input = new Scanner(System.in);
        int radius = input.nextInt();
        double pi = 3.14;
        double S = pi * radius * radius;
        int convert = (int) S;
        System.out.println(convert);
        
        
    }
}