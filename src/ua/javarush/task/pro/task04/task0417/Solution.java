package ua.javarush.task.pro.task04.task0417;

import java.util.Scanner;

/* 
Швидкість вітру
*/

public class Solution {
    public static void main(String[] args) {
        //напишіть тут ваш код
        Scanner input = new Scanner(System.in);
        int number = input.nextInt();
        int calculate = (int) Math.round(number * 3.6);
        System.out.println(calculate);
        
        
    }
}