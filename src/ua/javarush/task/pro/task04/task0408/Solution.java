package ua.javarush.task.pro.task04.task0408;

import java.util.Scanner;

/* 
Максимум серед уведених чисел
*/

public class Solution {
    public static void main(String[] args) {
        //напишіть тут ваш код
        Scanner input = new Scanner(System.in);

        int max = Integer.MIN_VALUE;

        while (input.hasNextInt()){
            int x = input.nextInt();

            if(x > max && x % 2 == 0){
                max = x;
            }
        }
        System.out.println(max);

    }
}