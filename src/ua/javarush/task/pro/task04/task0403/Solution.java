package ua.javarush.task.pro.task04.task0403;

import java.util.Scanner;

/* 
Підсумовування
*/

public class Solution {
    public static void main(String[] args) {
        //напишіть тут ваш код
        Scanner input = new Scanner(System.in);
        boolean flag = true;
        int summa = 0;
        while(flag){
            if(input.hasNextDouble()){
                int number = input.nextInt();
                summa += number;
            }else if(input.hasNextLine()){
                String word = input.nextLine();
                if(word.equals("ENTER")){
                    System.out.println(summa);
                    flag = false;
                }
                
            }
            
            
            
        }
    }
}