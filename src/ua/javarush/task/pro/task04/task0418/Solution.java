package ua.javarush.task.pro.task04.task0418;

import java.util.Scanner;

/* 
Склянка наполовину порожня чи наполовину повна?
*/

public class Solution {
    public static void main(String[] args) {
        double glass = 0.5;
        //напишіть тут ваш код
        Scanner input = new Scanner(System.in);
        
        if(input.nextBoolean() == true){
            int calculate1 = (int) Math.ceil(glass);
            System.out.println(calculate1);
        }else{
            int calculate2 = (int) Math.floor(glass);
            System.out.println(calculate2);
        }
        
        
        
        
        
    }
}