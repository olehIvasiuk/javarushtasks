package ua.javarush.task.pro.task02.task0214;

import java.util.Scanner;

/* 
Читання і перетворення рядків
*/

public class Solution {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String s1 = input.nextLine();
        String s2 = input.nextLine();
        String s3 = input.nextLine();
        
        System.out.println(s3);
        System.out.println(s2.toUpperCase());
        System.out.println(s1.toLowerCase());
    }
}
