package ua.javarush.task.pro.task02.task0215;

import java.util.Scanner;

/* 
Читання чисел
*/

public class Solution {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int numb1 = input.nextInt();
        int numb2 = input.nextInt();
        int numb3 = input.nextInt();

        System.out.println((numb1 + numb2 + numb3) / 3);

    }
}
