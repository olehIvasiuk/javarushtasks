package ua.javarush.task.pro.task11.task1109;

/* 
Об'єкти внутрішніх і вкладених класів
*/

public class Solution {
    public static void main(String[] args) {
        //напишіть тут ваш код
        Outer outer = new Outer();
        Outer.Inner inner = outer.new Inner();
        
//        Outer.Inner inner = new Outer.Inner();
        Outer.Nested nested = new Outer.Nested();
    }
}
