package ua.javarush.task.pro.task03.task0307;

import java.util.Scanner;

/* 
Працювати чи не працювати — отаке питання
*/

public class Solution {
    public static void main(String[] args) {
        //напишіть тут ваш код
        
        Scanner input = new Scanner(System.in);
        int number1 = input.nextInt();
        if (number1 < 20 || number1 > 60){
            System.out.println("можна не працювати");
        }
        
        
    }
}
