package ua.javarush.task.pro.task03.task0311;

import java.util.Scanner;

/* 
Висока точність
*/

public class Solution {
    public static void main(String[] args) {
        //напишіть тут ваш код
        
        Scanner input = new Scanner(System.in);
        double numb1 = input.nextDouble();
        double numb2 = input.nextDouble();
        
        if(Math.abs(numb1 - numb2) < 0.000001){
            System.out.println("числа рівні");

        }else {
            System.out.println("числа не рівні");

        }


        
    }
}
