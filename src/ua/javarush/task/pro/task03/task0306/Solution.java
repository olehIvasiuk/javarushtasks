package ua.javarush.task.pro.task03.task0306;

import java.util.Scanner;

/* 
Трикутник
*/

public class Solution {
    private static final String TRIANGLE_EXISTS = "трикутник існує";
    private static final String TRIANGLE_NOT_EXISTS = "трикутник не існує";

    public static void main(String[] args) {
        //напишіть тут ваш код
        Scanner input = new Scanner(System.in);
        int number1 = input.nextInt();
        int number2 = input.nextInt();
        int number3 = input.nextInt();
        
           
         if (number2 < number3 + number1 && number3 < number1 + number2 && number1 < number2 + number3){
            System.out.println(TRIANGLE_EXISTS);
        }else {
            System.out.println(TRIANGLE_NOT_EXISTS);
        }
        
        
        
        
    }
}
