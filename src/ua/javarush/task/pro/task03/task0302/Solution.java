package ua.javarush.task.pro.task03.task0302;

import java.util.Scanner;

/* 
Призовна кампанія
*/

public class Solution {
    public static void main(String[] args) {
        String militaryCommissar = ", з'явіться до військкомату";
        //напишіть тут ваш код
        Scanner input = new Scanner(System.in);
        String name = input.nextLine();
        int tempNumb = input.nextInt();
        if (tempNumb >= 18 && tempNumb <= 26) {
            System.out.println(name + militaryCommissar);
        }
    }
}
