package ua.javarush.task.pro.task03.task0312;

import java.util.Scanner;

/* 
Порівняймо рядки
*/

public class Solution {
    public static void main(String[] args) {
        //напишіть тут ваш код
        
        Scanner input = new Scanner(System.in);
        String s1 = input.nextLine();
        String s2 = input.nextLine();
        
        if(s1.equals(s2)){
            System.out.println("рядки однакові");

        }else{
            System.out.println("рядки різні");

        }
        
        //напишіть тут ваш код
    }
}
