package com.javarush.task.task19.task1923;

import java.io.*;

/* 
Слова с цифрами
*/

public class Solution {
    public static void main(String[] args) {

        try (BufferedReader file1 = new BufferedReader(new FileReader(args[0]));
             BufferedWriter file2 = new BufferedWriter(new FileWriter(args[1]))) {

            String line;
            String[] words;
            while ((line = file1.readLine()) != null) {
                words = line.split(" ");
                for (String word : words){
                    if (word.matches(".*\\d.*")){
                        file2.write(word + " ");
                    }
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
