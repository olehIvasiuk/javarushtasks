package com.javarush.task.task19.task1920;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;

/* 
Самый богатый
*/

public class Solution {
    public static void main(String[] args) {
        TreeMap<String, Double> map = new TreeMap<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(args[0]))) {
            double currentValue = 0;
            String line;
            while ((line = reader.readLine()) != null) {

                String[] keyValue = line.split(" ");
                String name = keyValue[0];
                double value = Double.parseDouble(keyValue[1]);

                if (map.containsKey(name)) {
                    currentValue = map.get(name);
                    map.put(name, value +currentValue);
                } else {
                    map.put(name, value);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        double maxCount = map.firstEntry().getValue();
        for (double value : map.values()) {
            if (value > maxCount) {
                maxCount = value;
            }

        }
        TreeSet<String> names = new TreeSet<>();
        for (String name : map.keySet()) {
            if (maxCount == map.get(name)) {
                names.add(name);
            }
        }

        for (String name : names) {
            System.out.println(name);
        }

    }
}
