package com.javarush.task.task19.task1922;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/* 
Ищем нужные строки
*/

public class Solution {
    public static List<String> words = new ArrayList<String>();

    static {
        words.add("файл");
        words.add("вид");
        words.add("В");
    }

    public static void main(String[] args) throws IOException {
        try (BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in))) {
            String filePath = consoleReader.readLine();
            String line;

            try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
                while (reader.ready()) {
                    int count = 0;
                    line = reader.readLine();
                    for (String s : line.split(" ")) {
                        if (words.contains(s)) {
                            count++;
                        }
                    if (count == 2) {
                        System.out.println(line);
                    }
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
