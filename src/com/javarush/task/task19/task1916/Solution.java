package com.javarush.task.task19.task1916;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/* 
Отслеживаем изменения
*/

public class Solution {
    public static List<LineItem> lines = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String oldFileName = reader.readLine();
        String newFileName = reader.readLine();
        reader.close();

        String lineOld;
        BufferedReader oldFileReader = new BufferedReader(new FileReader(oldFileName));
        List<String> stringsOld = new ArrayList<>();
        while ((lineOld = oldFileReader.readLine()) != null) {
            stringsOld.add(lineOld);
        }
        oldFileReader.close();

        String lineNew;
        BufferedReader newFileReader = new BufferedReader(new FileReader(newFileName));
        List<String> stringsNew = new ArrayList<>();
        while ((lineNew = newFileReader.readLine()) != null) {
            stringsNew.add(lineNew);
        }
        newFileReader.close();

        int oldFileLine = 0;
        int newFileLine = 0;

        while ((oldFileLine < stringsOld.size()) && (newFileLine < stringsNew.size())) {

            if (stringsOld.get(oldFileLine).equals(stringsNew.get(newFileLine))) {
                lines.add(new LineItem(Type.SAME, stringsOld.get(oldFileLine)));
                oldFileLine++;
                newFileLine++;
            } else if ((newFileLine + 1 < stringsOld.size()) && stringsOld.get(oldFileLine).equals(stringsNew.get(newFileLine + 1))) {
                    lines.add(new LineItem(Type.ADDED, stringsNew.get(newFileLine)));
                    newFileLine++;
            } else if ((newFileLine + 1 < stringsOld.size()) && stringsOld.get(oldFileLine + 1).equals(stringsNew.get(newFileLine))) {
                    lines.add(new LineItem(Type.REMOVED, stringsOld.get(oldFileLine)));
                    oldFileLine++;
            }
        }

        while (oldFileLine < stringsOld.size()){
            lines.add(new LineItem(Type.ADDED,stringsOld.get(oldFileLine)));
            oldFileLine++;
        }

        while (newFileLine < stringsNew.size()){
            lines.add(new LineItem(Type.REMOVED,stringsNew.get(newFileLine)));
            newFileLine++;
        }

//        for (int i = 0; i < stringsOld.size(); i++) {
//            for (int j = 0; j < stringsNew.size() - 1; j++) {
//                if (stringsOld.get(i).equals(stringsNew.get(j))){
//                    lines.add(new LineItem(Type.SAME, stringsOld.get(i)));
//                    break;
//                }else if (stringsOld.get(j).equals(stringsNew.get(i))){
//                    lines.add(new LineItem(Type.REMOVED, stringsOld.get(i)));
//                    break;
//                }else if (stringsNew.get(j).equals(stringsOld.get(i))){
//                    lines.add(new LineItem(Type.ADDED, stringsNew.get(j)));
//                    break;
//                }
//            }
//        }
        for (LineItem iu : lines) {
            System.out.println(iu.toString());
        }

    }

    public static enum Type {
        ADDED,        //добавлена новая строка
        REMOVED,      //удалена строка
        SAME          //без изменений
    }

    public static class LineItem {
        public Type type;
        public String line;

        public LineItem(Type type, String line) {
            this.type = type;
            this.line = line;
        }
    }
}
