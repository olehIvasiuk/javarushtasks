package com.javarush.task.task19.task1921;

import java.io.*;
import java.util.*;

/* 
Хуан Хуанович
*/

public class Solution {
    public static final List<Person> PEOPLE = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(args[0]))) {
            String newLine;
            while ((newLine = reader.readLine()) != null) {
                String name = "";
                String day = "";
                for (int i = 0; i < newLine.length(); i++) {
                    if (Character.isDigit(newLine.charAt(i))) {
                        name = newLine.substring(0, i - 1);
                        System.out.println(newLine.substring(i));
                        day = newLine.substring(i);
                        break;
                    }
                }
//                String[] partName = name.split("[-\\s]");
//                for (String n : partName) {
//                    name += n + " ";
//                }
                String[] date = day.split(" ");
                Calendar calendar = new GregorianCalendar(Integer.parseInt(date[2]), Integer.parseInt(date[1]), Integer.parseInt(date[0]));
                PEOPLE.add(new Person(name, calendar.getTime()));

            }
        }


    }
}
