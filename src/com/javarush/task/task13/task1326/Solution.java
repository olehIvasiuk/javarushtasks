package com.javarush.task.task13.task1326;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/* 
Сортировка четных чисел из файла
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        // напишите тут ваш код
FileInputStream file = new FileInputStream(new Scanner(System.in).nextLine());
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file));
        List<Integer> list = new ArrayList<>();

        String read;

        while ((read = bufferedReader.readLine()) != null) {
            int numb = Integer.parseInt(read);

            Collections.addAll(list, numb);
            Collections.sort(list);
        }
        for (Integer integer : list) {
            if (integer % 2 == 0) {
                System.out.println(integer);

            }
        }
        file.close();
        bufferedReader.close();

    }
}
