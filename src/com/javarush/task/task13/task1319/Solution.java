package com.javarush.task.task13.task1319;

import java.io.*;
import java.util.Scanner;

/* 
Писатель в файл с консоли
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        // напишите тут ваш код

        Scanner scanner = new Scanner(System.in);
        String path = scanner.nextLine();
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(path, true));
        boolean flag = true;
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = scanner.nextLine()) != null) {
            if(line.equalsIgnoreCase("exit")){
                sb.append(line);
                break;
            }
            sb.append(line).append("\n");
        }
        bufferedWriter.write(sb.toString());
        bufferedWriter.close();

    }
}
