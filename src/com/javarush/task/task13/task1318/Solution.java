package com.javarush.task.task13.task1318;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/* 
Чтение файла
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        // напишите тут ваш код

        FileInputStream fileInputStream = new FileInputStream(new Scanner(System.in).nextLine());
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
        String s = "";

        while ((s = bufferedReader.readLine()) != null) {
            System.out.println(s);

        }
fileInputStream.close();
        bufferedReader.close();
    }
}