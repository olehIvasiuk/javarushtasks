package com.javarush.task.task20.task2028;

import java.io.Serializable;
import java.util.*;

/* 
Построй дерево(1)
*/

public class CustomTree<E> extends AbstractList<String> implements Cloneable, Serializable {
    static ArrayList<Entry<String>> list = new ArrayList<>();
    private int size;

    Entry<E> root;

    public CustomTree() {
        root = new Entry<>("0");
    }

    String getParent(String s) {
        Queue<Entry<E>> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            Entry<E> entry = queue.poll();
            if (entry.elementName.equals(s)) {
                return entry.parent.elementName;
            } else {
                queue.add(entry.leftChild);
                queue.add(entry.rightChild);
            }
        }
        return null;
    }

    @Override
    public boolean add(String s) {
        Queue<Entry<E>> queue = new LinkedList<>();
        Entry<E> newEntry = new Entry<>(s);
        queue.add(root);
        while (!queue.isEmpty()) {
            boolean next = false;
            Entry<E> entry;
            if ((entry = queue.poll()) == null) {
                next = true;
            }
            if (next) {
                continue;
            } else if (entry.isAvailableToAddChildren()) {
                if (entry.availableToAddLeftChildren) {
                    entry.leftChild = newEntry;
                    entry.availableToAddLeftChildren = false;
                } else {
                    entry.rightChild = newEntry;
                    entry.availableToAddRightChildren = false;
                }
                newEntry.parent = entry;
                size++;
                return true;
            } else {
                queue.add(entry.leftChild);
                queue.add(entry.rightChild);
            }
        }
        return false;
    }

    public void checkChildren(){

    }

    @Override
    public boolean remove(Object o) {
        if (!(o instanceof String)) {
            throw new UnsupportedOperationException();
        } else {
            Queue<Entry<E>> queue = new LinkedList<>();
            queue.add(root);
            while (!queue.isEmpty()) {
                Entry<E> entry = queue.poll();
                if (entry.elementName.equals(o)) {
                    if (entry.parent.leftChild == entry) {
                        entry.parent.leftChild = null;
                        return true;
                    }
                    if (entry.parent.rightChild == entry) {
                        entry.parent.rightChild = null;
                        return true;
                    }
                }
                if (entry.leftChild == null) {
                    continue;
                } else {
                    queue.add(entry.leftChild);
                }
                if (entry.rightChild == null) {
                    continue;
                } else {
                    queue.add(entry.rightChild);
                }
            }
        }
        return false;
    }

    @Override
    public String get(int i) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int size() {
        boolean next = false;
        int size = 0;
        Queue<Entry<E>> entries = new LinkedList<>();
        entries.add(root);
        while (!entries.isEmpty()) {
            Entry<E> entry;

            if ((entry = entries.poll()) == null) {
                continue;
            }
            if (entry.leftChild != null) {
                size++;
            }
            if (entry.rightChild != null) {
                size++;
            }
            if (!entry.isAvailableToAddChildren()) {
                entries.add(entry.leftChild);
                entries.add(entry.rightChild);
            }
        }
        return size;
    }

    @Override
    public String set(int index, String element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void add(int index, String element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String remove(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(int index, Collection<? extends String> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<String> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void removeRange(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException();
    }

    static class Entry<T> implements Serializable {
        String elementName;
        boolean availableToAddLeftChildren, availableToAddRightChildren;
        Entry<T> parent;
        Entry<T> leftChild;
        Entry<T> rightChild;

        public Entry(String elementName) {
            this.elementName = elementName;
            availableToAddLeftChildren = true;
            availableToAddRightChildren = true;
        }

        public boolean isAvailableToAddChildren() {
            return (availableToAddLeftChildren || availableToAddRightChildren);
        }
    }
}
