package com.javarush.task.task20.task2028;

import java.util.List;

public class Solution {
    public static void main(String[] args) {
        CustomTree<String> list = new CustomTree();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        list.add("7");
        list.add("8");
        list.add("9");
        list.add("10");
        list.add("11");
        list.add("12");
        list.add("13");
        list.add("14");
        list.add("15");
        list.remove("1");
        System.out.println(list.size());
        list.remove("2");

        list.add("16");
        list.add("17");
        list.add("18");
        list.remove("2");
        System.out.println(list.size());
        list.add("19");
        list.add("20");
        list.add("21");
        list.add("22");
        list.add("23");
        System.out.println(list.size());
    }

}
