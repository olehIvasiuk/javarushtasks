package com.javarush.task.task20.task2021;

import java.io.*;

/* 
Сериализация под запретом
*/

 class Solution implements Serializable {
     static class SubSolution extends Solution {
        private void writeObject(ObjectOutputStream out) throws NotSerializableException {
            throw new NotSerializableException();
        }

        private void readObject(ObjectInputStream in) throws NotSerializableException {
            throw new NotSerializableException();
        }
    }

    public static void main(String[] args) {

    }
}
