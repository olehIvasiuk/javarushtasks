package com.javarush.task.task32.task3201;

import java.io.IOException;
import java.io.RandomAccessFile;

/* 
Запись в существующий файл
*/

public class Solution {
    public static void main(String... args) {

        String filePath = args[0];
        long seekNumber = Long.parseLong(args[1]);
        byte[] writeText = args[2].getBytes();

        try (RandomAccessFile file = new RandomAccessFile(filePath, "w")) {
            seekNumber = seekNumber > file.length() ? file.length() : seekNumber;

            file.seek(seekNumber);
            file.write(writeText);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
