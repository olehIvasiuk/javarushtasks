package com.javarush.task.task32.task3204;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Random;

/* 
Генератор паролей
*/

public class Solution {
    static char[] charsNumbers = getChars(48, 57);
    static char[] charsUpperSings = getChars(65, 90);
    static char[] charsLowerSings = getChars(97, 122);

    public static void main(String[] args) {
        ByteArrayOutputStream password = getPassword();
        System.out.println(password.toString());
    }

    public static ByteArrayOutputStream getPassword() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        String password = getUncheckedPassword();
        try {
            outputStream.write(password.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return outputStream;
    }

    private static String getUncheckedPassword() {
        Random r = new Random();
        char[] chars = new char[8];
        boolean containsNumber = false;
        boolean containsLower = false;
        boolean containsUpper = false;
        while (!(containsNumber && containsLower && containsUpper)) {
            for (int i = 0; i < chars.length; i++) {
                switch (r.nextInt(3)) {
                    case 0:
                        chars[i] = charsNumbers[r.nextInt(charsNumbers.length)];
                        containsNumber = true;
                        break;
                    case 1:
                        chars[i] = charsUpperSings[r.nextInt(charsUpperSings.length)];
                        containsUpper = true;
                        break;
                    case 2:
                        chars[i] = charsLowerSings[r.nextInt(charsLowerSings.length)];
                        containsLower = true;
                        break;
                }
            }
        }
        return new String(chars);
    }

    private static char[] getChars(int min, int max) {
        char[] chars;
        chars = new char[(max - min) + 1];
        int minCounter = min;
        for (int i = 0; i < chars.length; i++) {
            chars[i] = (char) (minCounter++);
        }
        return chars;
    }
}
