package com.javarush.task.task32.task3210;

import java.io.IOException;
import java.io.RandomAccessFile;
/* 
Используем RandomAccessFile
*/

public class Solution {
    public static void main(String... args) {
        byte[] bytes;
        try (RandomAccessFile file = new RandomAccessFile(args[0], "rw")) {
            long numberSeek = Long.parseLong(args[1]);
            String text = args[2];
            file.seek(numberSeek);
            bytes = new byte[text.length()];
            file.read(bytes, 0, text.length());
            String readText = new String(bytes);
            if (readText.equals(text)) {
                file.seek(file.length());
                file.write("true".getBytes());
            } else {
                file.seek(file.length());
                file.write("false".getBytes());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
