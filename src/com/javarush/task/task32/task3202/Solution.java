package com.javarush.task.task32.task3202;

import java.io.*;

/* 
Читаем из потока
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        StringWriter writer = getAllDataFromInputStream(new FileInputStream("testFile.log"));
        System.out.println(writer.toString());
    }

    public static StringWriter getAllDataFromInputStream(InputStream is) throws IOException {
        StringWriter writer = new StringWriter();
        if (is != null) {
//            String line;
            char[] bytes = new char[256];
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
                int count = 0;
                while ((count = reader.read(bytes)) != -1) {
                    writer.write(bytes, 0, count);
                }
            }
        }
        return writer;
    }
}
