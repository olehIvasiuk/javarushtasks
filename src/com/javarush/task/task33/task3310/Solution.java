package com.javarush.task.task33.task3310;

import com.javarush.task.task33.task3310.strategy.HashMapStorageStrategy;
import com.javarush.task.task33.task3310.strategy.StorageStrategy;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Solution {
    public static void main(String[] args) {
        long number = 1000L;
        testStrategy(new HashMapStorageStrategy(), number);
    }

    public static Set<Long> getIds(Shortener shortener, Set<String> strings) {
        Set<Long> set = new HashSet<>();
        for (String s : strings) {
            set.add(shortener.getId(s));
        }
        return set;
    }

    public static Set<String> getStrings(Shortener shortener, Set<Long> keys) {
        Set<String> set = new HashSet<>();
        for (Long l : keys) {
            set.add(shortener.getString(l));
        }
        return set;
    }

    public static void testStrategy(StorageStrategy strategy, long elementsNumber) {
        Helper.printMessage(strategy.getClass().getSimpleName());

        Set<String> set = new HashSet<>();
        for (int i = 0; i < elementsNumber; i++) {
            set.add(Helper.generateRandomString());
        }

        Shortener shortener = new Shortener(strategy);

        Date dateBefore = new Date();
        Set<Long> ids = getIds(shortener, set);
        Date dateAfter = new Date();
        long difference = dateAfter.getTime() - dateBefore.getTime();
        Helper.printMessage("Метод отработав - " + difference);

        dateBefore = new Date();
        Set<String> strings = getStrings(shortener, ids);
        dateAfter = new Date();

        difference = dateBefore.getTime() - dateAfter.getTime();
        Helper.printMessage("Метод отработав - " + difference);

         if (set.equals(strings)){
             Helper.printMessage("Тест пройден.");
         }else {
             Helper.printMessage("Тест не пройден.");
         }
    }


}
