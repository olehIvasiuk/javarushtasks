package com.javarush.task.task31.task3113;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/*
Что внутри папки?
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String filePath = reader.readLine();
        Path path = Paths.get(filePath);
        if (!Files.isDirectory(path)) {
            System.out.println(filePath + " - не папка");
            return;
        } else {
             AtomicInteger dirCount = new AtomicInteger();

             AtomicInteger countFiles = new AtomicInteger();
             AtomicLong size = new AtomicLong();
            Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                    if (!dir.equals(Paths.get(filePath))) {
                        dirCount.incrementAndGet();
                    }                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    countFiles.incrementAndGet();
                    size.addAndGet(attrs.size());
                    return FileVisitResult.CONTINUE;
                }
            });
            System.out.println("Всего папок - " + (dirCount.get() - 1));
            System.out.println("Всего файлов - " + countFiles.get());
            System.out.println("Общий размер - " + size.get());
        }
    }

    private static void walkFile(String folderPath) {

    }
}
