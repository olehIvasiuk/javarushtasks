package com.javarush.task.task31.task3112;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;

/* 
Загрузчик файлов
*/

public class Solution {

    public static void main(String[] args) throws IOException {
        Path passwords = downloadFile("https://javarush.ru/testdata/secretPasswords.txt", Paths.get("D:/MyDownloads"));

        for (String line : Files.readAllLines(passwords)) {
            System.out.println(line);
        }
    }

    public static Path downloadFile(String urlString, Path downloadDirectory) throws IOException {
        // implement this method
        String[] fileName =  urlString.split("/");
        Path pathDownload = downloadDirectory.resolve(fileName[fileName.length - 1]);

        try {
            URL url = new URL(urlString);
            InputStream inputStream = url.openStream();
            Path path = Files.createTempFile("temp-", ".tmp");
            Files.copy(inputStream,path,StandardCopyOption.REPLACE_EXISTING);
            
            Files.move(path,pathDownload);
        }catch (IOException e){
            e.printStackTrace();
        }
        return pathDownload;

    }
}
