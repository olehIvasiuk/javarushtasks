package com.javarush.task.task31.task3101;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;

/* 
Проход по дереву файлов
*/

public class Solution {
    public static void main(String[] args) {
        String path = args[0];
        String resultFileAbsolutePath = args[1];

        File path1 = new File(resultFileAbsolutePath);
        File def = new File(path1.getParentFile() + "/allFilesContent.txt");

//        File renamedFile = new File(path1.getAbsolutePath() + "allFilesContent.txt");

        FileUtils.renameFile(path1,def);
        try {
            Map<String, byte[]> validFiles = getFileTree(path);
            try (FileOutputStream outputStream = new FileOutputStream(def)){
                for (Map.Entry<String, byte[]> needToWrite : validFiles.entrySet()) {
//                    byte bytesFromFile = needToWrite.getValue() + "\n".getBytes();
                    outputStream.write(needToWrite.getValue());
                    outputStream.write("\n".getBytes());
                }
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }


    public static Map<String, byte[]> getFileTree(String root) throws IOException {
        Queue<File> queue = new ArrayDeque<>();
        queue.add(new File(root));
        Map<String, byte[]> list = new HashMap<>();
        while (!queue.isEmpty()) {
            File file = queue.poll();
            File[] files = file.listFiles();
            if (files != null) {
                for (File file1 : files) {
                    if (file1.isDirectory()) {
                        queue.add(file1);
                    } else {
                        System.out.println(file1.length());
                        if (file1.length() <= 50) {
                            list.put(file1.getAbsolutePath(), Files.readAllBytes(file1.toPath()));
                        }
                    }
                }
            }
        }
        return list;
    }
}
