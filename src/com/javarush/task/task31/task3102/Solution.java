package com.javarush.task.task31.task3102;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;

/* 
Находим все файлы
*/

public class Solution {
    public static List<String> getFileTree(String root) throws IOException {
        Queue<File> queue = new ArrayDeque<>();
        queue.add(new File(root));
        List<String> list = new ArrayList<>();
        while (!queue.isEmpty()) {
            File file = queue.poll();
            File[] files = file.listFiles();
            if (files != null) {
                for (File file1 : files) {
                    if (file1.isDirectory()) {
                        queue.add(file1);
                    } else {
                        list.add(file1.getAbsolutePath());
                    }
                }
            }
        }
        return list;
    }

    public static void main(String[] args) throws IOException {
        getFileTree("\"C:\\Users\\oleg_\\Desktop\\TestsA\\Tests\"");
    }
}
