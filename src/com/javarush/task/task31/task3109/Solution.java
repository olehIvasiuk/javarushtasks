package com.javarush.task.task31.task3109;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/* 
Читаем конфиги
*/

public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        Properties properties = solution.getProperties("src/com/javarush/task/task31/task3109/properties.xml");
        properties.list(System.out);

        properties = solution.getProperties("src/com/javarush/task/task31/task3109/properties.txt");
        properties.list(System.out);

        properties = solution.getProperties("src/com/javarush/task/task31/task3109/notExists");
        properties.list(System.out);
    }

    public Properties getProperties(String fileName) {
        Properties properties = new Properties();
        Path path = Paths.get(fileName);
        String fileEx = "";

        try (InputStream inputStream = new FileInputStream(fileName)) {
            String p = path.toAbsolutePath().toString();
            int index = p.lastIndexOf(File.separator);
            if (index >= 0) {
                int index2 = p.lastIndexOf(".");
                if (index2 > index) {
                    p = p.substring(index2 + 1);
                    fileEx = p;
                }
                fileEx = p;
            } else {
                fileEx = p;
            }
//            C:\Users\oleg_\javarush\3357315\javarush-project\src\com\javarush\task\task31\task3109\properties.xml
            if ("xml".equals(fileEx)) {
                properties.loadFromXML(inputStream);
            } else {
                properties.load(inputStream);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


        return properties;
    }
}
