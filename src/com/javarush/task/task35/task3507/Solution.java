package com.javarush.task.task35.task3507;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.Set;

/* 
ClassLoader - что это такое?
*/

public class Solution {
    public static void main(String[] args) {
        String s1 = Solution.class.getProtectionDomain().getCodeSource().getLocation().getPath() + Solution.class.getPackage().getName().replaceAll("[.]", "/") + "/data";
        Set<? extends Animal> allAnimals = getAllAnimals(Solution.class.getProtectionDomain().getCodeSource().getLocation().getPath() + Solution.class.getPackage().getName().replaceAll("[.]", "/") + "/data");
        System.out.println(allAnimals);
    }

    public static Set<? extends Animal> getAllAnimals(String pathToAnimals) {
        Set<Animal> set = new HashSet<>();
        if (!pathToAnimals.endsWith("\\") && !pathToAnimals.endsWith("/")) {
            pathToAnimals = pathToAnimals + "/";
        }
        File file = new File(pathToAnimals);
        String[] files = file.list(new FilenameFilter() {
            @Override
            public boolean accept(File file, String s) {
                return s.toLowerCase().endsWith(".class");
            }
        });

        ClassLoader loader = new ClassLoader() {
            @Override
            protected Class<?> findClass(String name) throws ClassNotFoundException {
                byte[] bytes;
                try {
                    FileInputStream fileInputStream = new FileInputStream(name);
                    bytes = new byte[(int) new File(name).length()];
                    while (fileInputStream.read(bytes) != -1) {
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                return defineClass(null, bytes, 0, bytes.length);
            }
        };

        for (String file1 : files) {
            boolean intTrue = false;
            boolean modTrue = false;
            String classPath = file1.substring(0, file1.length() - 6);
            try {
                Class<?> clazz = loader.loadClass(pathToAnimals + classPath + ".class");
                Class[] interfaces = clazz.getInterfaces();
                for (Class animal : interfaces) {
                    if (Animal.class == animal) {
                        intTrue = true;
                    }
                }

                Constructor[] constructor = clazz.getConstructors();
                for (Constructor cons : constructor) {
                    if (Modifier.toString(cons.getModifiers()).equals("public") && cons.getParameters().length == 0) {
                        modTrue = true;
                    }
                }
                if (intTrue && modTrue) {
                    set.add((Animal) clazz.newInstance());
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
        return set;
    }
}
