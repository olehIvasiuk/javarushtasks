package com.javarush.task.task39.task3913;

import java.nio.file.Paths;
import java.util.Date;

public class Solution {
    public static void main(String[] args) {
        LogParser logParser = new LogParser(Paths.get("C:\\Users\\oleg_\\JavaRush\\3357315\\javarush-project\\src\\com\\javarush\\task\\task39\\task3913\\logs"));

//        System.out.println(logParser.getNumberOfUniqueIPs(null, null));
//        System.out.println(logParser.getUniqueIPs(null, null));
//        System.out.println(logParser.getIPsForUser("Vasya Pupkin", null, null));
//        System.out.println(logParser.getIPsForEvent(Event.LOGIN, new Date("3/2/2029"), new Date("2/2/2014")));
//        System.out.println(logParser.getIPsForStatus(Status.OK, null, null));

//        System.out.println(logParser.getAllUsers());
//        System.out.println(logParser.getNumberOfUsers(null, null));
//        System.out.println(logParser.getUsersForIP("127.0.0.1", new Date("3/2/2029"),  new Date("1/2/2020")));
//        System.out.println(logParser.getDownloadedPluginUsers(new Date("10/13/2013"), new Date("8/30/2012")));
//        System.out.println(logParser.getLoggedUsers(new Date("3/2/2029"), new Date("1/2/2022")));
//        System.out.println(logParser.getSolvedTaskUsers(new Date("10/13/2019"), new Date("3/18/2016"), 1));

//        System.out.println(logParser.getDatesForUserAndEvent("Vasya Pupkin", Event.SOLVE_TASK,new Date("1/3/2014"),new Date("10/14/2021")));

//        System.out.println(logParser.getDateWhenUserLoggedFirstTime("Vasya Pupkin", null, null));
//        System.out.println(logParser.getDateWhenUserLoggedFirstTime("Vasya Pupkin", new Date("11/11/2012"), new Date("11/3/2022")));
        System.out.println(logParser.getDateWhenUserSolvedTask("Vasya Pupkin", 18, new Date("11/11/2022"), new Date("11/3/2012")));


    }
}