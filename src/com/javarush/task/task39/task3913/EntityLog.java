package com.javarush.task.task39.task3913;

import java.util.Date;

public class EntityLog {
    private String ip;
    private String user;
    private Date date;
    private Event event;
    private int eventParameter;
    private Status status;

    public EntityLog(String ip, String user, Date date, Event event, int eventParameter, Status status) {
        this.ip = ip;
        this.user = user;
        this.date = date;
        this.event = event;
        this.eventParameter = eventParameter;
        this.status = status;
    }

    public String getIp() {
        return ip;
    }

    public String getUser() {
        return user;
    }

    public Date getDate() {
        return date;
    }

    public Event getEvent() {
        return event;
    }

    public int getEventParameter() {
        return eventParameter;
    }

    public Status getStatus() {
        return status;
    }
}
