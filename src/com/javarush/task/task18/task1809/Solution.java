package com.javarush.task.task18.task1809;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* 
Реверс файла
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String firstPath = br.readLine();
        String secondPath = br.readLine();

        try (FileInputStream file1 = new FileInputStream(firstPath);
             FileOutputStream file2 = new FileOutputStream(secondPath)) {
            byte[] readByte = new byte[file1.available()];

            int read = file1.read(readByte);
            for (int i = read; i > 0; i--) {
                file2.write(readByte[i -1]);
            }

        }catch (IOException e){
            e.printStackTrace();
        }

    }
}
