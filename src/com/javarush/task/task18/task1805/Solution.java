package com.javarush.task.task18.task1805;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/* 
Сортировка байт
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        Set<Integer> sorted = new TreeSet<>();
        StringBuilder sb = new StringBuilder();
        try (FileInputStream file = new FileInputStream(new BufferedReader(new InputStreamReader(System.in)).readLine())) {
            while (file.available() > 0) {
                sorted.add(file.read());
            }
            for (Integer i : sorted) {
                sb.append(i).append(" ");
            }
        }
        System.out.println(sb);

    }
}
