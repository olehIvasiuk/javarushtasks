package com.javarush.task.task18.task1808;

import java.io.*;
import java.nio.file.Path;

/* 
Разделение файла
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String firstPath = br.readLine();
        String secondPath = br.readLine();
        String thirdPath = br.readLine();

        try (FileInputStream file1 = new FileInputStream(firstPath);
             FileOutputStream file2 = new FileOutputStream(secondPath);
             FileOutputStream file3 = new FileOutputStream(thirdPath)) {
            int halfOfFile = (file1.available() + 1) / 2;
            int numberOfByte = 0;
            while (file1.available() > 0) {
                if (numberOfByte < halfOfFile) {
                    file2.write(file1.read());
                    numberOfByte++;
                } else {
                    file3.write(file1.read());
                }
            }
        }

    }
}
