package com.javarush.task.task18.task1818;

import java.io.*;

/* 
Два в одном
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String firstPath = br.readLine();
        String secondPath = br.readLine();
        String thirdPath = br.readLine();

        try (FileOutputStream file1 = new FileOutputStream(firstPath, true);
             FileInputStream file2 = new FileInputStream(secondPath);
             FileInputStream file3 = new FileInputStream(thirdPath)) {

            int i;
            while (file2.available() > 0 || file3.available() > 0) {
                if (file2.available() > 0) {
                    i = file2.read();
                    file1.write(i);
                }else if (file3.available() > 0){
                    i = file3.read();
                    file1.write(i);

                }
            }


        }


    }
}
