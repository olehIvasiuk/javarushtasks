package com.javarush.task.task18.task1817;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/* 
Пробелы
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        int sings = 0;
        int space = 0;

        try (FileReader fileReader = new FileReader(args[0])) {

            while (fileReader.ready()) {
                int readedChar = fileReader.read();
                sings++;
                if (readedChar == (int) ' ') space++;
            }
        }
        if (sings != 0) {
            double result = (double) space / sings * 100;
            System.out.printf("%.2f", result);
        }
    }
}
