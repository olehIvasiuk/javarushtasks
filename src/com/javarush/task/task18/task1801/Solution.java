package com.javarush.task.task18.task1801;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;

/* 
Максимальный байт
*/

public class Solution {
    public static void main(String[] args) throws Exception {

        try (FileInputStream file = new FileInputStream(new BufferedReader(new InputStreamReader(System.in)).readLine())) {
            int maxByte = 0;
            while (file.available() > 0) {
                byte bytes = (byte) file.read();
                if (bytes > maxByte){
                    maxByte = bytes;
                }
            }
                System.out.println(maxByte);

        } catch (FileNotFoundException f) {
            f.printStackTrace();
        }


    }
}
