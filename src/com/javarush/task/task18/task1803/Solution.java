package com.javarush.task.task18.task1803;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;

/* 
Самые частые байты
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        Map<Integer, Integer> repeatBytes = new HashMap<>();

        try (FileInputStream file = new FileInputStream(new BufferedReader(new InputStreamReader(System.in)).readLine())) {
            while (file.available() > 0) {
                int b = file.read();
                if (!repeatBytes.containsKey(b)) {
                    repeatBytes.put(b, 1);

                } else {
                    repeatBytes.put(b, repeatBytes.get(b) + 1);
                }

            }
            int maxCount = repeatBytes.values().stream().max(Integer::compareTo).get();
            for (Map.Entry<Integer, Integer> i :repeatBytes.entrySet()){
               if (i.getValue() == maxCount ){
                   System.out.print(i.getKey() + " ");
               }
            }
        }
    }
}
