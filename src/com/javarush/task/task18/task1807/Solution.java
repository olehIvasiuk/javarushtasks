package com.javarush.task.task18.task1807;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Подсчет запятых
*/

public class Solution {
    public static void main(String[] args) {
        try (FileInputStream file = new FileInputStream(new BufferedReader(new InputStreamReader(System.in)).readLine())) {

            char signComma = ',';
            int countSign = 0;
            while (file.available() > 0) {
                if (file.read() == (int) signComma) {
                    countSign++;
                }
            }
            System.out.println(countSign);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
