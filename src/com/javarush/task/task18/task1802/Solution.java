package com.javarush.task.task18.task1802;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/* 
Минимальный байт
*/

public class Solution {
    public static void main(String[] args) throws Exception {

        try (FileInputStream file = new FileInputStream(new BufferedReader(new InputStreamReader(System.in)).readLine())) {
            int minByte = Integer.MAX_VALUE;
            while (file.available() > 0) {
                byte bytes = (byte) file.read();
                if (bytes < minByte){
                    minByte = bytes;
                }
            }
            System.out.println(minByte);

        }
    }
}
