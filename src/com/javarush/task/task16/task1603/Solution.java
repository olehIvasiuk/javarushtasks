package com.javarush.task.task16.task1603;

import java.util.ArrayList;
import java.util.List;

/* 
Список и нити
*/

public class Solution {
    public static volatile List<Thread> list = new ArrayList<Thread>(5);

    public static void main(String[] args) {
        //напишите тут ваш код
        SpecialThread sp1 = new SpecialThread();
        sp1.run();
        SpecialThread sp2 = new SpecialThread();
        sp2.run();
        SpecialThread sp3 = new SpecialThread();
        sp3.run();
        SpecialThread sp4 = new SpecialThread();
        sp4.run();
        SpecialThread sp5 = new SpecialThread();
        sp5.run();

        list.add(new Thread(sp1));
        list.add(new Thread(sp2));
        list.add(new Thread(sp3));
        list.add(new Thread(sp4));
        list.add(new Thread(sp5));
    }

    public static class SpecialThread implements Runnable {
        public void run() {
            System.out.println("it's a run method inside SpecialThread");
        }
    }
}
