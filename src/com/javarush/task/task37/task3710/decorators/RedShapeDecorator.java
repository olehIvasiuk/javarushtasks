package com.javarush.task.task37.task3710.decorators;

import com.javarush.task.task37.task3710.shapes.Shape;

public class RedShapeDecorator extends ShapeDecorator implements Shape {
    Shape decShape;

    public RedShapeDecorator(Shape decoratedShape) {
        super(decoratedShape);
        this.decShape = decoratedShape;
    }

    private void setBorderColor(Shape shape){
        System.out.printf("Setting the border color for %s to red.%n", shape.getClass().getSimpleName());
    }

    @Override
    public void draw() {
        setBorderColor(decShape);
        decShape.draw();
    }
}
