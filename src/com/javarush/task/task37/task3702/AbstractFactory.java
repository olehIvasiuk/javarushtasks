package com.javarush.task.task37.task3702;

public interface AbstractFactory {
    <T extends Human> Human getPerson(int age);
}
