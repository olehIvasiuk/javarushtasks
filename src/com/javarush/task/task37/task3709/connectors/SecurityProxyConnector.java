package com.javarush.task.task37.task3709.connectors;

import com.javarush.task.task37.task3709.security.SecurityChecker;

public class SecurityProxyConnector implements Connector{
    SimpleConnector connector;

    SecurityChecker checker;

    public SecurityProxyConnector(String connector) {
        this.connector = new SimpleConnector(connector);
    }

    @Override
    public void connect() {
        if (checker.performSecurityCheck()){
            connector.connect();
        }
    }
}
