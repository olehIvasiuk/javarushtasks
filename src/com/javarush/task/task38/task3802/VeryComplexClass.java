package com.javarush.task.task38.task3802;

/* 
Проверяемые исключения (checked exception)
*/

import sun.misc.Unsafe;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class VeryComplexClass {
    public void veryComplexMethod() throws CloneNotSupportedException {
        //напишите тут ваш код
        this.clone();
    }

    public static void main(String[] args) {
    }
}
