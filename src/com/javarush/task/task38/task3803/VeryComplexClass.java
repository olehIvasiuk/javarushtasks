package com.javarush.task.task38.task3803;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/* 
Runtime исключения (unchecked exception)
*/

public class VeryComplexClass {
    public void methodThrowsClassCastException() {
        List list = new ArrayList();
        ((LinkedList) list).add(new String("4e34"));
        list.add(23);
    }

    public void methodThrowsNullPointerException() {
        int[] arr = null;
        int i = arr.length;
    }

    public static void main(String[] args) {

    }
}
