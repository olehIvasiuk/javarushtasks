package com.javarush.task.task15.task1514;

import java.util.HashMap;
import java.util.Map;

/* 
Статики-1
*/

public class Solution {
    public static Map<Double, String> labels = new HashMap<Double, String>();

    public static void main(String[] args) {
        System.out.println(labels);
    }
    static {
        labels.put(543.7,"346");
        labels.put(3.4,"sdfgv");
        labels.put(233.8,"sdv");
        labels.put(12.4,"ghed");
        labels.put(15.78,"yer46");
    }
}
