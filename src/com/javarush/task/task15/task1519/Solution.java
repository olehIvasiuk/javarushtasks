package com.javarush.task.task15.task1519;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* 
Разные методы для разных типов
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        //напиште тут ваш код
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String input;
        while (!(input = bufferedReader.readLine()).equals("exit")) {
            if (isShort(input)) {
                print(Short.parseShort(input));
            } else if (isInteger(input)) {
                print(Integer.valueOf(input));
            } else if (isDouble(input)) {
                print(Double.parseDouble(input));
            } else if (input.equals("exit")) {
                break;
            } else {
                print(input);
            }
        }
    }

    private static boolean isInteger(String s) throws NumberFormatException {
        try {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private static boolean isDouble(String s) throws NumberFormatException {
        try {
            Double.parseDouble(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private static boolean isShort(String s) throws NumberFormatException {
        try {
            short sh = Short.parseShort(s);
            if(sh > 0 && sh < 128){
                return true;
            }
        } catch (NumberFormatException e) {
            return false;
        }
        return false;
    }

    public static void print(Double value) {
        System.out.println("Это тип Double, значение " + value);
    }

    public static void print(String value) {
        System.out.println("Это тип String, значение " + value);
    }

    public static void print(short value) {
        System.out.println("Это тип short, значение " + value);
    }

    public static void print(Integer value) {
        System.out.println("Это тип Integer, значение " + value);
    }
}
