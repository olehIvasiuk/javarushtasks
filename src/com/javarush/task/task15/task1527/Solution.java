package com.javarush.task.task15.task1527;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* 
Парсер реквестов
*/

public class Solution {
    List<String> params = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String url = reader.readLine();
        //напишите тут ваш код
        Solution solution = new Solution();
        solution.trim(url);

    }


    public void trim(String url) {
        ArrayList<String> obnjParams = new ArrayList<>();
        for (String partBefore : url.split("\\?")) {
            if (partBefore.contains("http")) {
                continue;
            }
            for (String param : partBefore.split("&")) {
                if (param.contains("obj")) {
                    obnjParams.add(param);
                }
                System.out.println(param.split("=")[0]);
            }

        }
        for (String obj : obnjParams) {
            String part = obj.split("=")[1];
            if (!isParseble(part)) {
                alert(part);
            }
        }

    }


    private static boolean isParseble(String s) {
        try {
            double value = Double.parseDouble(s);
            alert(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static void alert(double value) {
        System.out.println("double: " + value);
    }

    public static void alert(String value) {
        System.out.println("String: " + value);
    }
}

