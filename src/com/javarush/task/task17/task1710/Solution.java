package com.javarush.task.task17.task1710;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/* 
CRUD
*/

public class Solution {
    public static List<Person> allPeople = new ArrayList<Person>();

    static {
        allPeople.add(Person.createMale("Иванов Иван", new Date()));  //сегодня родился    id=0
        allPeople.add(Person.createMale("Петров Петр", new Date()));  //сегодня родился    id=1
    }

    private static SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
    private static SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);

    public static void main(String[] args) throws ParseException {
        //напишите тут ваш код

        Person person;
        Date bDate;
        switch (args[0]) {
            case "-c":
                bDate = dateFormat1.parse(args[3]);
                if (args[2].equals("м")) {
                    person = Person.createMale(args[1], bDate);
                } else {
                    person = Person.createFemale(args[1], bDate);
                }
                allPeople.add(person);
                System.out.println(allPeople.size() - 1);
                break;
            case "-r":
                person = allPeople.get(Integer.parseInt(args[1]));
                if (person != null) {
                    System.out.println(person.getName() + " " + (person.getSex() == Sex.FEMALE ? "ж" : "м") + " " + dateFormat2.format(person.getBirthDate()));
                }
                break;
            case "-u":
                bDate = dateFormat2.parse(args[4]);
                int id = Integer.parseInt(args[1]);
                person = allPeople.get(id);
                if (person == null) {
                    throw new IllegalArgumentException();
                }
                person.setName(args[2]);
                person.setBirthDate(bDate);
                person.setSex((Objects.equals(args[3], "м") ? Sex.MALE : Sex.FEMALE));
                allPeople.set(id, person);
                break;
            case "-d":
                Person person1 = allPeople.get(Integer.parseInt(args[1]));
                person1.setSex(null);
                person1.setName(null);
                person1.setBirthDate(null);

                break;
        }

    }
}
