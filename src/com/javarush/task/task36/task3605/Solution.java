package com.javarush.task.task36.task3605;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.TreeSet;

/* 
Использование TreeSet
*/

public class Solution {

    public static void main(String[] args) throws IOException {
        TreeSet<Character> set = new TreeSet<>();
        int count = 0;
        String s;
        try (BufferedReader reader = new BufferedReader(new FileReader(args[0]))) {
            while ((s = reader.readLine()) != null) {
                for (char character : s.toLowerCase().toCharArray()) {
                    if (character >= 97 && character <= 122) {
                        set.add(character);
                    }
                }
            }
            reader.close();
            for (Character c : set) {
                System.out.print(c);
                count++;
                if (count == 5) {
                    break;
                }
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
